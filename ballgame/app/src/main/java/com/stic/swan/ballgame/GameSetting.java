package com.stic.swan.ballgame;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.swan.ballgame.R;
import com.tsengvn.typekit.TypekitContextWrapper;

public class GameSetting extends AppCompatActivity {
    int vibrate; // 0 || 1
    int music; // 0 || 1
    private Switch VibrateSwitch, MusicSwitch ;
    private Button ResetLevelBtn;
    Context context;
    UserInfo userInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_setting);
        this.setFinishOnTouchOutside(true);
        context = this;

        userInfo = new UserInfo(this);
        WindowManager.LayoutParams  layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags  = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount  = 0.1f;
        getWindow().setAttributes(layoutParams);
        getWindow().getAttributes().width   = (int)(getApplicationContext().getResources().getDisplayMetrics().widthPixels * 0.8);
        getWindow().getAttributes().height  = (int)(getApplicationContext().getResources().getDisplayMetrics().heightPixels * 0.3);
        getWindow().setBackgroundDrawableResource(R.drawable.layout_radious_white);

        vibrate = userInfo.getVibrate();
        music = userInfo.getMusic();
        ResetLevelBtn = (Button)findViewById(R.id.ResetLevelBtn);
        VibrateSwitch = (Switch)findViewById(R.id.VibrateSwitch);
        MusicSwitch = (Switch)findViewById(R.id.MusicSwitch);

        if(vibrate == 1){
            VibrateSwitch.setChecked(true);
        }else{
            VibrateSwitch.setChecked(false);
        }

        if(music == 1){
            MusicSwitch.setChecked(true);
        }else{
            MusicSwitch.setChecked(false);
        }

        VibrateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true){
                    VibrateSwitch.setChecked(true);
                    vibrate = 1;
                }else{
                    VibrateSwitch.setChecked(false);
                    vibrate = 0;
                }
                userInfo.saveVibrate(vibrate);
            }
        });

        MusicSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true){
                    MusicSwitch.setChecked(true);
                    userInfo.saveMusic(1);
                    ((Menu)Menu.context).startMusic();
                }else{
                    MusicSwitch.setChecked(false);
                    userInfo.saveMusic(0);
                    ((Menu)Menu.context).stopMusic();
                }

            }
        });

        ResetLevelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserInfo userInfo = new UserInfo(context);
                String params = "name="+ userInfo.getUsername()+"&" +
                        "id="+ userInfo.getUserid()+"&"+
                        "image="+ userInfo.getUserimage()+"&"+
                        "stage="+userInfo.loadLevel();

//                Network  network = new Network();
//                NetworkCallback networkCallback = new NetworkCallback() {
//                    @Override
//                    public void callbackMethod(String code, String resultData) {
//                        if(code.equalsIgnoreCase("success")){
//                            Toast.makeText(context, "Save Complete!", Toast.LENGTH_LONG).show();
//                        }else{
//                            Toast.makeText(context, "Network error", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                };
//                network.requestURL(network.SET_USER_DATA + params, networkCallback);
                SharedPreferences pref = getSharedPreferences("GAME", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("LEVEL", 0);
                editor.commit();
            }
        });
    }
    @Override
    protected void onPause() {
        super.onPause();
        ((Menu)Menu.context).stopMusic();
    }
    @Override
    protected void onResume() {
        super.onResume();
        ((Menu)Menu.context).startMusic();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.dropdown, R.anim.dropdown_exit);
    }
}
