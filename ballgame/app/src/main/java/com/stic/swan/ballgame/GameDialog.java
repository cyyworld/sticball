package com.stic.swan.ballgame;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.swan.ballgame.R;

/**
 * Created by swan on 2018-03-15.
 */

public class GameDialog extends Dialog implements View.OnClickListener{
    private static final int LAYOUT = R.layout.game_dialog;
    private TextView game_dialog_description;
    private ImageView game_dialog_image;
    private Context context;
    private Button game_dialog_confirm;
    private Boolean IS_FINISH = true;

    public GameDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public GameDialog(Context context,String name){
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        this.getWindow().setWindowAnimations(R.style.DropdownAnimation);
        this.getWindow().setBackgroundDrawableResource(R.drawable.layout_radious_white);

        this.game_dialog_description = (TextView)findViewById(R.id.game_dialog_description);
        this.game_dialog_image = (ImageView)findViewById(R.id.game_dialog_image);
        this.game_dialog_confirm = (Button)findViewById(R.id.game_dialog_confirm);
        this.game_dialog_confirm.setOnClickListener(this);
        this.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if(IS_FINISH){
                    ((Activity)context).finish();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        cancel();
    }
    public void is_finish(boolean a){
        this.IS_FINISH = a;
    }
    public void setDesc(String s){
        this.game_dialog_description.setText(s);
    }

    public void setImg(String type, String data){
        if(type.equalsIgnoreCase("CHARACTER")){

            int level = (int)(Math.random()*12);
            switch (level){
            case 0 :
                this.game_dialog_image.setImageResource(R.drawable.swan);
                break;
            case 1 :
                this.game_dialog_image.setImageResource(R.drawable.swan2);
                break;
            case 2 :
                this.game_dialog_image.setImageResource(R.drawable.charles);
                break;
            case 3 :
                this.game_dialog_image.setImageResource(R.drawable.charles2);
                break;
            case 4 :
                this.game_dialog_image.setImageResource(R.drawable.charles3);
                break;
            case 5 :
                this.game_dialog_image.setImageResource(R.drawable.tobam);
                break;
            case 6 :
                this.game_dialog_image.setImageResource(R.drawable.tobam2);
                break;
            case 7 :
                this.game_dialog_image.setImageResource(R.drawable.tobam3);
                break;
            case 8 :
                this.game_dialog_image.setImageResource(R.drawable.myeongin);
                break;
            case 9 :
                this.game_dialog_image.setImageResource(R.drawable.myeongin2);
                break;
            case 10 :
                this.game_dialog_image.setImageResource(R.drawable.myeongin3);
                break;
            case 11 :
                this.game_dialog_image.setImageResource(R.drawable.soo);
                break;
                default:break;
            }
        }else if(type.equalsIgnoreCase("GAMESTART")){//START 안내 팝업
            if(data.equalsIgnoreCase("STAGE1")) {
                this.game_dialog_image.setImageResource(R.drawable.circle_game_selected);
            }else if(data.equalsIgnoreCase("STAGE2")) {
                this.game_dialog_image.setImageResource(R.drawable.item_shuffle);
            }else if(data.equalsIgnoreCase("STAGE3")) {
                this.game_dialog_image.setImageResource(R.drawable.two_ball_dialog);
            }else if(data.equalsIgnoreCase("STAGE4")) {
                this.game_dialog_image.setImageResource(R.drawable.item_bomb);
            }else if(data.equalsIgnoreCase("STAGE5")) {
                this.game_dialog_image.setImageResource(R.drawable.three_ball_dialog);
            }
        }
    }

}
