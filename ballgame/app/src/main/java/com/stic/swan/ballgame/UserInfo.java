package com.stic.swan.ballgame;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

/**
 * Created by swan on 2018-03-27.
 */

public class UserInfo {
    Context context;

    public UserInfo(@NonNull Context context) {
        this.context = context;
    }
    public int loadLevel() {
        SharedPreferences pref = context.getSharedPreferences("GAME", Activity.MODE_PRIVATE);
        int level = pref.getInt("LEVEL", 0);
        return level;
    }
    public void saveUserInfo(String name, String image, String id){
        SharedPreferences pref = ((Activity)context).getSharedPreferences("USERINFO", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("NAME", name);
        editor.putString("IMAGE", image);
        editor.putString("ID", id);
        editor.commit();
    }
    public String getUsername() {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("USERINFO", Activity.MODE_PRIVATE);
           return pref.getString("NAME","");
    }
    public String getUserimage() {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("USERINFO", Activity.MODE_PRIVATE);
        return pref.getString("IMAGE","");
    }
    public String getUserid() {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("USERINFO", Activity.MODE_PRIVATE);
        return pref.getString("ID","");
    }
    public void saveVibrate(int vibrate) {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("SETTING", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("VIBRATE", vibrate);
        editor.commit();

    }
    public int getVibrate(){
        SharedPreferences pref = ((Activity)context).getSharedPreferences("SETTING", Activity.MODE_PRIVATE);
        int vibrate = pref.getInt("VIBRATE", 1);
        return vibrate;
    }
    public void saveMusic(int vibrate) {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("SETTING", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("MUSIC", vibrate);
        editor.commit();

    }
    public int getMusic(){
        SharedPreferences pref = ((Activity)context).getSharedPreferences("SETTING", Activity.MODE_PRIVATE);
        int musicplay = pref.getInt("MUSIC", 1);
        return musicplay;
    }
}
