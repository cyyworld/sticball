package com.stic.swan.ballgame;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.media.MediaPlayer;

import com.example.swan.ballgame.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tsengvn.typekit.TypekitContextWrapper;

import static com.example.swan.ballgame.R.anim.dropdown_exit;

public class Menu extends AppCompatActivity {
    public static Context context;
    TextView stage[] = new TextView[50];
    BitmapDrawable d_arr[] = new BitmapDrawable[10];
    HorizontalScrollView menu_scrollView;
    int level;
    int STAGE_MARGIN;
    int stage_size;
    int STAGE_COUNT = 30;
    int LAYOUT_COUNT = 3;
    int LAYOUT_WIDTH;
    UserInfo userInfo;
    MediaPlayer mp;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        context = this;

        DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "jua_font.ttf");

        menu_scrollView = (HorizontalScrollView)findViewById(R.id.menu_scrollView);
        userInfo = new UserInfo(this);
        int device_width = dm.widthPixels;
        int device_height = dm.heightPixels;
        if(hasSoftMenu()){
            device_height = device_height - getSoftMenuHeight();
        }
        stage_size = (int)(device_width*0.2);
        STAGE_MARGIN = stage_size;
        LAYOUT_WIDTH = (STAGE_COUNT/LAYOUT_COUNT)*(stage_size+STAGE_MARGIN)+STAGE_MARGIN;
        float density_scale = getResources().getDisplayMetrics().density;
        level = userInfo.loadLevel();
        Drawable dr;
        dr = ContextCompat.getDrawable(this, R.drawable.stage_on);
        d_arr[0] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(((BitmapDrawable) dr).getBitmap(), stage_size, stage_size, true));

        dr = ContextCompat.getDrawable(this, R.drawable.stage_off);
        d_arr[1] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(((BitmapDrawable) dr).getBitmap(), stage_size, stage_size, true));
        int height_interval;

        RelativeLayout menu_relative_layout = (RelativeLayout)findViewById(R.id.menu_relative_layout);
        RelativeLayout.LayoutParams relativeParams1 = new RelativeLayout.LayoutParams(LAYOUT_WIDTH, ViewGroup.LayoutParams.MATCH_PARENT);
        RelativeLayout.LayoutParams relativeParams2 = new RelativeLayout.LayoutParams(LAYOUT_WIDTH, ViewGroup.LayoutParams.MATCH_PARENT);
        RelativeLayout.LayoutParams relativeParams3 = new RelativeLayout.LayoutParams(LAYOUT_WIDTH, ViewGroup.LayoutParams.MATCH_PARENT);

        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        LinearLayout stageLayout1, stageLayout2, stageLayout3;
        stageLayout1= new LinearLayout(this);
        stageLayout2= new LinearLayout(this);
        stageLayout3= new LinearLayout(this);

        relativeParams1.setMargins(0,0,0,0);
        relativeParams2.setMargins(LAYOUT_WIDTH,0,0,0);
        relativeParams3.setMargins(LAYOUT_WIDTH*2,0,0,0);

        stageLayout1.setLayoutParams(relativeParams1);
        stageLayout2.setLayoutParams(relativeParams2);
        stageLayout3.setLayoutParams(relativeParams3);

        stageLayout1.setBackgroundResource(R.drawable.menu_background1);
        stageLayout2.setBackgroundResource(R.drawable.menu_background2);
        stageLayout3.setBackgroundResource(R.drawable.menu_background3);

        viewParams.setMargins(STAGE_MARGIN,0, STAGE_MARGIN,0);
        for(int i = 0; i<STAGE_COUNT; i++){
            height_interval = (int)(Math.random()*200) - 100;
            stage[i] = new TextView(this);
            stage[i].setTypeface(typeFace);
            stage[i].setText(Integer.toString(i+1));
            stage[i].setHeight(stage_size);
            stage[i].setWidth(stage_size);
            stage[i].setTextSize(9.f * density_scale);
            stage[i].setGravity(Gravity.CENTER_VERTICAL);
            stage[i].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            stage[i].setY((float)(device_height*0.2)+height_interval);
            stage[i].setId(i);

            if(i <= level){
                stage[i].setBackgroundResource(R.drawable.circle_menu_selected);
                stage[i].setTextColor(Color.rgb(255,255,255));
            }else if (i==level){
                stage[i].setBackgroundResource(R.drawable.circle_menu_current_level);
                stage[i].setTextColor(Color.rgb(255,255,255));
            }else{
                stage[i].setBackgroundResource(R.drawable.circle_menu_not_selected);
                stage[i].setTextColor(Color.rgb(255,255,255));
            }
            stage[i].setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int id = v.getId();
                if(id <= level){
                    TextView view = (TextView) v;
                    Intent intent = new Intent(Menu.this, GameInfo.class);
                    intent.putExtra("GAME_LEVEL", view.getText());
                    ActivityOptions options = ActivityOptions.makeCustomAnimation(Menu.this, R.anim.dropdown, dropdown_exit);
                    Menu.this.startActivity(intent,options.toBundle());
                }
                }
            });

            if(i < 10){
                stageLayout1.addView(stage[i]);
                stage[i].setX((i+1)*STAGE_MARGIN);
            }else if(i < 20){
                stageLayout2.addView(stage[i]);
                stage[i].setX((i-9)*STAGE_MARGIN);
            }else{
                stageLayout3.addView(stage[i]);
                stage[i].setX((i-19)*STAGE_MARGIN);
            }
        }
        ImageButton gameSettingBtn = (ImageButton)findViewById(R.id.gameSettingBtn);
        gameSettingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, GameSetting.class));
            }
        });

        Button menu_ranking_btn = (Button)findViewById(R.id.menu_ranking_btn);
        menu_ranking_btn.setVisibility(View.INVISIBLE);
        menu_ranking_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, Ranking.class));
            }
        });

        menu_relative_layout.addView(stageLayout1);
        menu_relative_layout.addView(stageLayout2);
        menu_relative_layout.addView(stageLayout3);

        int size, shape, cloudY = 0;
        for(int i=0; i<25; i++){//cloud
            size = (int)(stage_size*(Math.random()*(2)+1.7));
            cloudY = (int)(Math.random()*(size/2));
            TextView cloud = new TextView(this);
            cloud.setBackgroundResource(R.drawable.menu_circle);
            cloud.setX(size*(i)*0.85f);
            cloud.setY(device_height*0.6f+cloudY);
            cloud.setWidth(size);
            cloud.setHeight(size);
            menu_relative_layout.addView(cloud);
        }

        for(int i = 0; i<10; i++){//mountain
            size = (stage_size*7);
            TextView cloud = new TextView(this);
            cloud.setBackgroundResource(R.drawable.menu_triangle2);
            cloud.setX((float)(Math.random()*LAYOUT_WIDTH*LAYOUT_COUNT) - size);
            cloud.setY(device_height-size);
            cloud.setWidth(size);
            cloud.setHeight(size);
            menu_relative_layout.addView(cloud);
        }

        for(int i = 0; i<15; i++){//mountain
            size = (stage_size*5);
            TextView cloud = new TextView(this);
            cloud.setBackgroundResource(R.drawable.menu_triangle);
            cloud.setX((float)(Math.random()*LAYOUT_WIDTH*LAYOUT_COUNT) - size);
            cloud.setY(device_height-size);
            cloud.setWidth(size);
            cloud.setHeight(size);
            menu_relative_layout.addView(cloud);
        }

    }
    public void startMusic(){
        int music = userInfo.getMusic();
        if(music ==0)return;
        if(mp == null){
            mp = MediaPlayer.create(this,R.raw.mysong);
            mp.setLooping(true);
            mp.start();
        }else{
            mp.start();
        }
    }
    public void stopMusic(){
        int music = userInfo.getMusic();
        if(mp != null){
            mp.pause();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        stopMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        menu_scrollView.post(new Runnable() {
            @Override
            public void run() {
                menu_scrollView.scrollTo((level)*(stage_size + (int)(STAGE_MARGIN)),0);
                menu_scrollView.post(null);
            }
        });
        level = userInfo.loadLevel();
        for(int i =0; i<STAGE_COUNT; i++){
            if(i < level){
                stage[i].setBackgroundResource(R.drawable.circle_menu_selected);
                stage[i].setTextColor(Color.rgb(255,255,255));
            }else if (i==level){
                stage[i].setBackgroundResource(R.drawable.circle_menu_current_level);
                stage[i].setTextColor(Color.rgb(255,255,255));
            }else{
                stage[i].setBackgroundResource(R.drawable.circle_menu_not_selected);
                stage[i].setTextColor(Color.rgb(255,255,255));
            }
        }
        int music = userInfo.getMusic();
        if(music == 1){
            startMusic();
        }else{
            stopMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopMusic();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    private boolean hasSoftMenu() {
        //메뉴버튼 유무
        boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();

        //뒤로가기 버튼 유무
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

        if (!hasMenuKey && !hasBackKey) { // lg폰 소프트키일 경우
            return true;
        } else { // 삼성폰 등.. 메뉴 버튼, 뒤로가기 버튼 존재
            return false;
        }
    }

    private int getSoftMenuHeight() {
        Resources resources = this.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        int deviceHeight = 0;
        int statusHeight = 0;
        if (resourceId > 0) {
            deviceHeight = resources.getDimensionPixelSize(resourceId);
            Log.i("TEST","navigation_bar_height : "+deviceHeight );
        }
        resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if(resourceId >0 ){
            statusHeight = resources.getDimensionPixelSize(resourceId);
            Log.i("TEST","status_bar_height : "+statusHeight );
        }
        deviceHeight -= statusHeight;
        return deviceHeight;
    }
}
