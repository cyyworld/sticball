package com.stic.swan.ballgame;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.swan.ballgame.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.tsengvn.typekit.TypekitContextWrapper;

public class GameInfo extends AppCompatActivity {
    private String intentString;
    int POINT_VAL = 50;//1POINT 당 10점
    private ImageView gameCharacterView;
    private TextView gameInfoCharacterName;
    private InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_info);

        this.setFinishOnTouchOutside(true);
        WindowManager.LayoutParams  layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags  = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount  = 0.1f;
        getWindow().setAttributes(layoutParams);
        getWindow().getAttributes().width   = (int)(getApplicationContext().getResources().getDisplayMetrics().widthPixels * 0.8);
        getWindow().getAttributes().height  = (int)(getApplicationContext().getResources().getDisplayMetrics().heightPixels * 0.6);
        getWindow().setBackgroundDrawableResource(R.drawable.layout_radious_white);
        intentString = getIntent().getStringExtra("GAME_LEVEL");
        TextView gameStart = (TextView)findViewById(R.id.gameStartBtn);

        gameCharacterView = (ImageView)findViewById(R.id.gameCharacterView);
        gameInfoCharacterName = (TextView)findViewById(R.id.gameInfoCharacterName);
        loadCharacter();

//        gameCharacterView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(GameInfo.this, GameCharacter.class);
//                GameInfo.this.startActivity(intent);
//            }
//        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1442158865320372/2922088450");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        gameStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("STIC", "The interstitial wasn't loaded yet.");
                    changeActivity();
                }
            }
        });

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                changeActivity();
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
                changeActivity();
            }
        });
        TextView gameStageInfo = (TextView)findViewById(R.id.gameStageInfo);
        gameStageInfo.setText("Goal " + Integer.parseInt(intentString)*POINT_VAL + " point");
        loadScore();
    }

    public  void changeActivity(){
        int level = Integer.parseInt(intentString);
        Intent intent;
        if(level < 5){
            intent = new Intent(GameInfo.this, Game1.class);
        }else if(level < 10){
            intent = new Intent(GameInfo.this, Game2.class);
        }else if(level < 15){
            intent = new Intent(GameInfo.this, Game3.class);
        }else if(level < 20){
            intent = new Intent(GameInfo.this, Game4.class);
        }else{
            intent = new Intent(GameInfo.this, Game5.class);
        }

        intent.putExtra("GAME_LEVEL", intentString);
        GameInfo.this.startActivity(intent);
        finish();
    }
    @Override
    protected void onPause() {
        super.onPause();
        ((Menu)Menu.context).stopMusic();
    }
    @Override
    protected void onResume() {
        super.onResume();
        loadScore();
        loadCharacter();
        ((Menu)Menu.context).startMusic();
    }
    private void loadCharacter() {
//        SharedPreferences pref = getSharedPreferences("GAME", Activity.MODE_PRIVATE);
//        String character = pref.getString("CHARACTER", "SWAN");
//        gameInfoCharacterName.setText(character);
//        if(character.equalsIgnoreCase("SWAN")){
//            gameCharacterView.setImageResource(R.drawable.swan);
//        }else if(character.equalsIgnoreCase("TOBAM")){
//            gameCharacterView.setImageResource(R.drawable.tobam);
//        }else if(character.equalsIgnoreCase("ILGU")){
//            gameCharacterView.setImageResource(R.drawable.swan);
//        }

        int level = (int)(Math.random()*12);
        switch (level){
            case 0 :
                gameInfoCharacterName.setText("SWAN");
                gameCharacterView.setImageResource(R.drawable.swan);
                break;
            case 1 :
                gameInfoCharacterName.setText("SWAN");
                gameCharacterView.setImageResource(R.drawable.swan2);
                break;
            case 2 :
                gameInfoCharacterName.setText("CHARLES");
                gameCharacterView.setImageResource(R.drawable.charles);
                break;
            case 3 :
                gameInfoCharacterName.setText("CHARLES");
                gameCharacterView.setImageResource(R.drawable.charles2);
                break;
            case 4 :
                gameInfoCharacterName.setText("CHARLES");
                gameCharacterView.setImageResource(R.drawable.charles3);
                break;
            case 5 :
                gameInfoCharacterName.setText("TOBAM");
                gameCharacterView.setImageResource(R.drawable.tobam);
                break;
            case 6 :
                gameInfoCharacterName.setText("TOBAM");
                gameCharacterView.setImageResource(R.drawable.tobam2);
                break;
            case 7 :
                gameInfoCharacterName.setText("TOBAM");
                gameCharacterView.setImageResource(R.drawable.tobam3);
                break;
            case 8 :
                gameInfoCharacterName.setText("MYEONGIN");
                gameCharacterView.setImageResource(R.drawable.myeongin);
                break;
            case 9 :
                gameInfoCharacterName.setText("MYEONGIN");
                gameCharacterView.setImageResource(R.drawable.myeongin2);
                break;
            case 10 :
                gameInfoCharacterName.setText("MYEONGIN");
                gameCharacterView.setImageResource(R.drawable.myeongin3);
                break;
            case 11 :
                gameInfoCharacterName.setText("SOO");
                gameCharacterView.setImageResource(R.drawable.soo);
                break;
                default:break;
        }
    }
    private void loadScore() {
        SharedPreferences pref = getSharedPreferences("LEVEL"+intentString, Activity.MODE_PRIVATE);
        int score = pref.getInt("score", 0);
        TextView bestScore = (TextView) findViewById(R.id.gameBestScore);
        bestScore.setText("Best score : " + Integer.toString(score)+" point");
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.dropdown, R.anim.dropdown_exit);
    }
}
