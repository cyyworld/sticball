package com.stic.swan.ballgame;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.FrameLayout;

import com.kakao.auth.ISessionCallback;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;

public class SessionCallback implements ISessionCallback {
    String TAG = "STIC";
    Context context;
    FrameLayout kakaoLoginBtn;
    public SessionCallback(@NonNull Context context) {
        this.context = context;
    }
    @Override
    public void onSessionOpened() {
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Log.e(TAG, "error message=" + errorResult);
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.d(TAG, "onSessionClosed1 =" + errorResult);
            }

            @Override
            public void onNotSignedUp() {
                //카카오톡 회원이 아닐시
                Log.d(TAG, "onNotSignedUp ");
            }

            @Override
            public void onSuccess(UserProfile result) {
                UserInfo userInfo = new UserInfo(context);
                userInfo.saveUserInfo(result.getNickname(),result.getProfileImagePath(), result.getEmail());

                String params = "name="+ result.getNickname()+"&" +
                        "id="+ result.getEmail()+"&"+
                        "image="+ result.getProfileImagePath()+"&"+
                        "stage="+userInfo.loadLevel();
                Network  network = new Network();
                NetworkCallback networkCallback = new NetworkCallback() {
                    @Override
                    public void callbackMethod(String code, String resultData) {
                        Log.d(TAG,"###########"+resultData);
                        Intent mainIntent = new Intent(context, Menu.class);
                        context.startActivity(mainIntent);
                        ((Activity)context).finish();
                    }
                };
                network.requestURL(network.SET_USER_DATA+params, networkCallback);
            }
        });
    }

    @Override
    public void onSessionOpenFailed(KakaoException exception) {
        if(exception != null) {
            Logger.e(exception);
        }
    }
}