package com.stic.swan.ballgame;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.swan.ballgame.R;
import com.tsengvn.typekit.TypekitContextWrapper;

public class GameCharacter extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_character);
        this.setFinishOnTouchOutside(true);
        WindowManager.LayoutParams  layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags  = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount  = 0.5f;
        getWindow().setAttributes(layoutParams);
        getWindow().getAttributes().width   = (int)(getApplicationContext().getResources().getDisplayMetrics().widthPixels * 1.f);
        getWindow().getAttributes().height  = (int)(getApplicationContext().getResources().getDisplayMetrics().heightPixels * 0.5);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private int pageNum;
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_game_character, container, false);
            pageNum = getArguments().getInt(ARG_SECTION_NUMBER);
            TextView character_name = (TextView) rootView.findViewById(R.id.character_name);
            TextView character_info = (TextView) rootView.findViewById(R.id.character_info);
            ImageView character_image = (ImageView)rootView.findViewById(R.id.character_image);
            Button character_select_btn = (Button)rootView.findViewById(R.id.character_select_btn);
            if(pageNum == 1){
                character_image.setImageResource(R.drawable.swan);
                character_name.setText("swan");
                character_info.setText("Item Percentage X 2");
            }else if(pageNum == 2){
                character_image.setImageResource(R.drawable.tobam);
                character_name.setText("tobam");
                character_info.setText("Speed Down");
            }else if(pageNum == 3){
                character_image.setImageResource(R.drawable.swan);
                character_name.setText("ilgu");
                character_info.setText("Item effect X 2");
            }
            character_select_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name;
                    switch (pageNum){
                        case 1 : name = "SWAN";
                            break;
                        case 2 : name = "TOBAM";
                            break;
                        case 3 : name = "ILGU";
                            break;
                     default: name = "SWAN";
                            break;
                    }
                   SharedPreferences pref = ((GameCharacter)getActivity()).getSharedPreferences("GAME",Activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("CHARACTER", name);
                    editor.commit();
                    ((GameCharacter)getActivity()).finish();
                }
            });
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
