package com.stic.swan.ballgame;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.example.swan.ballgame.R;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.google.android.gms.ads.MobileAds;

public class Splash extends AppCompatActivity {
    int SPLASH_DISPLAY_LENGTH = 1000;
    String TAG ="STIC";
    SessionCallback callback;
    FrameLayout kakaoLoginBtn;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       //getHashKey();
        this.context = this;
        kakaoLoginBtn = (FrameLayout)findViewById(R.id.kakaoLoginBtn);
        kakaoLoginBtn.setVisibility(View.INVISIBLE);

//        callback = new SessionCallback(this);
//        Session.getCurrentSession().addCallback(callback);
//        Session.getCurrentSession().checkAndImplicitOpen();
//       requestMe();
        MobileAds.initialize(this, "ca-app-pub-1442158865320372~7816619655");
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent mainIntent = new Intent(context, Menu.class);
                context.startActivity(mainIntent);
                finish();
            }
        }, 3000);

    }

    private void getHashKey(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.example.swan.ballgame", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d(TAG,"key_hash="+Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Session.getCurrentSession().removeCallback(callback);
    }

    public void requestMe() {
        //유저의 정보를 받아오는 함수

        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Log.e(TAG, "error message=" + errorResult);
                kakaoLoginBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.d(TAG, "onSessionClosed1 =" + errorResult);
                kakaoLoginBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNotSignedUp() {
                //카카오톡 회원이 아닐시
                Log.d(TAG, "onNotSignedUp ");
                kakaoLoginBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(UserProfile result) {
            }
        });
    }

}
