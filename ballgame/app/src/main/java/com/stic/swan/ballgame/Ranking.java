package com.stic.swan.ballgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.swan.ballgame.R;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class Ranking extends AppCompatActivity {
    String TAG = "STIC";
    LinearLayout ranking_vertical_layout;
    Context context;
    int layoutHeight;
    int objectWidth;
    UserInfo userInfo;
    Typeface tp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        context = this;
        this.setFinishOnTouchOutside(true);
        WindowManager.LayoutParams  layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags  = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount  = 0.1f;
        getWindow().setAttributes(layoutParams);
        getWindow().getAttributes().width   = (int)(getApplicationContext().getResources().getDisplayMetrics().widthPixels * 0.8);
        getWindow().getAttributes().height  = (int)(getApplicationContext().getResources().getDisplayMetrics().heightPixels * 0.8);
        getWindow().setBackgroundDrawableResource(R.drawable.layout_radious_white);
        layoutHeight =  (int)(getApplicationContext().getResources().getDisplayMetrics().heightPixels * 0.8)/7;
        objectWidth = (int)(getApplicationContext().getResources().getDisplayMetrics().widthPixels * 0.8)/4;
        ranking_vertical_layout = (LinearLayout)findViewById(R.id.ranking_vertical_layout);
        tp = Typeface.createFromAsset(getAssets(), "jua_font.ttf");
        userInfo = new UserInfo(context);
        Network  network = new Network();
        NetworkCallback networkCallback = new NetworkCallback() {
            @Override
            public void callbackMethod(String code, String resultData) {
                Log.d(TAG,"###########"+resultData);

                try {
                    JSONObject resultJSON = new JSONObject(resultData);
                    JSONArray data_arr =  resultJSON.getJSONArray("data");
                    for(int i =0; i<data_arr.length(); i++){
                            JSONObject data = data_arr.getJSONObject(i);
                            if(data==null)break;
                            LinearLayout horizental_layout = new LinearLayout(context);
                            horizental_layout.setOrientation(LinearLayout.HORIZONTAL);
                            LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, layoutHeight);
                            viewParams.setMargins(0,20,0,20);

                            LinearLayout.LayoutParams imgviewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            imgviewParams.gravity = Gravity.CENTER_VERTICAL;

                            Drawable dr = ContextCompat.getDrawable(context, R.drawable.swan);
                            BitmapDrawable bd = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap( ((BitmapDrawable) dr).getBitmap(),layoutHeight/2, layoutHeight/2, true));

                            ImageView imgView = new ImageView(context);
                            Glide.with(getApplicationContext()).load(data.getString("image")).placeholder(bd).override(layoutHeight/2,layoutHeight/2).bitmapTransform(new CropCircleTransformation(context)).dontAnimate().into(imgView);
                            imgView.setLayoutParams(imgviewParams);

                            TextView order = new TextView(context);
                            TextView name = new TextView(context);
                            TextView stage = new TextView(context);

                            RelativeLayout orderLayout = new RelativeLayout(context);
                            RelativeLayout.LayoutParams orderParams = new RelativeLayout.LayoutParams(objectWidth,layoutHeight);
                            orderParams.setMargins(20,0,0,0);
                            orderLayout.setLayoutParams(orderParams);

                            order.setWidth(40);
                            order.setHeight(40);

                            if(data.getString("id").equalsIgnoreCase(userInfo.getUserid())){
                                order.setBackgroundResource(R.drawable.circle_ranking_me);
                            }else{
                                order.setBackgroundResource(R.drawable.circle_ranking);
                            }
                            name.setWidth(objectWidth);
                            name.setHeight(layoutHeight);
                            stage.setWidth(objectWidth);
                            stage.setHeight(layoutHeight);

                            order.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            name.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            stage.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            order.setTextColor(Color.WHITE);
                            name.setTextColor(Color.BLACK);
                            stage.setTextColor(Color.BLACK);

                            order.setTypeface(tp);
                            name.setTypeface(tp);
                            stage.setTypeface(tp);

                            order.setGravity(Gravity.CENTER_VERTICAL);
                            stage.setGravity(Gravity.CENTER_VERTICAL);
                            order.setGravity(Gravity.CENTER_VERTICAL);


                            order.setText(Integer.toString(i+1));
                            name.setText(data.getString("name"));
                            stage.setText(data.getString("stage"));
                            order.setTextSize(20);
                            name.setTextSize(20);
                            stage.setTextSize(25);

                            ranking_vertical_layout.addView(horizental_layout);
                            orderLayout.addView(order);
                            horizental_layout.addView(orderLayout);
                            horizental_layout.addView(imgView);
                            horizental_layout.addView(name);
                            horizental_layout.addView(stage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        network.requestURL(network.GET_USER_DATA, networkCallback);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onResume() {
        super.onResume();
        ((Menu)Menu.context).startMusic();
    }
    @Override
    protected void onPause() {
        super.onPause();
        ((Menu)Menu.context).stopMusic();
    }
}
