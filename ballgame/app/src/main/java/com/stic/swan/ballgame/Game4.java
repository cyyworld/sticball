package com.stic.swan.ballgame;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.swan.ballgame.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tsengvn.typekit.TypekitContextWrapper;

public class Game4 extends AppCompatActivity {
    String TAG = "STIC";
    GameUtil gameUtil;
    UserInfo userInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game1);

        Log.d(TAG,"#####onCreate######");
        DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
        gameUtil = new GameUtil(this);
        userInfo = new UserInfo(this);
        gameUtil.vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        gameUtil.device_width = dm.widthPixels;
        gameUtil.device_height = dm.heightPixels;
        gameUtil.ball_sizse = (gameUtil.device_width/6);
        gameUtil.TITLE_HEIGHT = gameUtil.device_height/6;
        gameUtil.LEVEL = Integer.parseInt(getIntent().getStringExtra("GAME_LEVEL"));
        gameUtil.mainLayout = (RelativeLayout) findViewById(R.id.game_main1);
        gameUtil.scoreView = (TextView) findViewById(R.id.scoreView1);
        gameUtil.density_scale = getResources().getDisplayMetrics().density;
        gameUtil.gameDialog = new GameDialog(this);
        gameUtil.gameDialog.setCancelable(true);
        gameUtil.typeFace = Typeface.createFromAsset(getAssets(), "jua_font.ttf");
        gameUtil.IS_GO_TO_SKY = false;
        gameUtil.IS_PAUSE_BALL = false;
        gameUtil.IS_VIBRATE_SETTING = userInfo.getVibrate();
        gameUtil.GOAL_POINT = gameUtil.LEVEL*5;
        gameUtil.makePointAnimation();

        gameUtil.setGameLevel();

        ImageView game_background = (ImageView)findViewById(R.id.game_background1);
        Glide.with(this).load(R.drawable.game_background).override(gameUtil.device_width,gameUtil.device_height).centerCrop().into(game_background);
        Drawable dr;
        dr = ContextCompat.getDrawable(this, R.drawable.sky_ball);
        gameUtil.d_arr[0] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(((BitmapDrawable) dr).getBitmap(),  gameUtil.ball_sizse*2, gameUtil.ball_sizse*2, true));
        gameUtil.d_arr[0].setGravity(Gravity.TOP);

        dr = ContextCompat.getDrawable(this, R.drawable.item_stop);
        gameUtil.d_arr[1] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap( ((BitmapDrawable) dr).getBitmap(), gameUtil.ball_sizse, gameUtil.ball_sizse, true));
        gameUtil.d_arr[1].setGravity(Gravity.TOP);

        dr = ContextCompat.getDrawable(this, R.drawable.item_up);
        gameUtil.d_arr[2] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap( ((BitmapDrawable) dr).getBitmap(), gameUtil.ball_sizse, gameUtil.ball_sizse, true));
        gameUtil.d_arr[2].setGravity(Gravity.TOP);

        dr = ContextCompat.getDrawable(this, R.drawable.item_shuffle);
        gameUtil.d_arr[3] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap( ((BitmapDrawable) dr).getBitmap(), gameUtil.ball_sizse, gameUtil.ball_sizse, true));
        gameUtil.d_arr[3].setGravity(Gravity.TOP);

        dr = ContextCompat.getDrawable(this, R.drawable.item_heart);
        gameUtil.d_arr[4] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap( ((BitmapDrawable) dr).getBitmap(), gameUtil.ball_sizse, gameUtil.ball_sizse, true));
        gameUtil.d_arr[4].setGravity(Gravity.TOP);

        dr = ContextCompat.getDrawable(this, R.drawable.item_bomb);
        gameUtil.d_arr[5] = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap( ((BitmapDrawable) dr).getBitmap(), gameUtil.ball_sizse, gameUtil.ball_sizse, true));
        gameUtil.d_arr[5].setGravity(Gravity.TOP);

        for(int i = 0; i<4; i++){
            gameUtil.itemBox[i] = new TextView(this);
            gameUtil.itemBox[i].setTypeface(gameUtil.typeFace);
            gameUtil.itemBox[i].setBackground(gameUtil.d_arr[1+i]);
            gameUtil.itemBox[i].setWidth(gameUtil.ball_sizse);
            gameUtil.itemBox[i].setHeight(gameUtil.ball_sizse + (gameUtil.ball_sizse/2));
            gameUtil.itemBox[i].setTextSize(7.0f * gameUtil.density_scale);
            gameUtil.itemBox[i].setTextColor(Color.parseColor("#FFFFFF"));
            gameUtil.itemBox[i].setText(Integer.toString(3));
            gameUtil.itemBox[i].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            gameUtil.itemBox[i].setGravity(Gravity.BOTTOM);
            gameUtil.itemBox[i].setClickable(true);
            gameUtil.itemBox[i].setFocusable(true);
            gameUtil.itemBox[i].setX((gameUtil.device_width/4)*i + ((gameUtil.device_width/4) - gameUtil.ball_sizse)/3 );
            gameUtil.itemBox[i].setY((gameUtil.TITLE_HEIGHT - gameUtil.ball_sizse)/2);
            gameUtil.mainLayout.addView(gameUtil.itemBox[i]);
        }

        gameUtil.itemBox[0].setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                gameUtil.setVibrate(gameUtil.VIBRATE_MID);
                TextView view = (TextView)v;
                gameUtil.scaleAnim(view);
                if(gameUtil.IS_GO_TO_SKY == true)return;
                int item_count = Integer.parseInt(view.getText().toString());
                if(item_count > 0){
                    gameUtil.IS_PAUSE_BALL = true;
                    gameUtil.pauseAnimation();
                    view.setText(Integer.toString(item_count-1));
                }
            }
        });

        gameUtil.itemBox[1].setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                gameUtil.setVibrate(gameUtil.VIBRATE_MID);
                TextView view = (TextView)v;
                gameUtil.scaleAnim(view);
                if(gameUtil.IS_GO_TO_SKY == true)return;
                int item_count = Integer.parseInt(view.getText().toString());
                if(item_count > 0){
                    gameUtil.IS_PAUSE_BALL = false;
                    gameUtil.IS_GO_TO_SKY = true;
                    gameUtil.pauseAnimation();
                    goToSky(0);
                    view.setText(Integer.toString(item_count-1));
                }
            }
        });

        gameUtil.itemBox[2].setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                gameUtil.setVibrate(gameUtil.VIBRATE_MID);
                if(gameUtil.IS_GO_TO_SKY == true)return;
                TextView view = (TextView)v;
                gameUtil.scaleAnim(view);
                if(gameUtil.IS_GO_TO_SKY == true)return;
                int item_count = Integer.parseInt(view.getText().toString());
                if(item_count > 0){
                    gameUtil.USE_INVERSE_ITEM = 5;
                    view.setText(Integer.toString(item_count-1));
                    gameUtil.inverseItem(gameUtil.textViews[gameUtil.POINT], gameUtil.POINT, gameUtil.viewAnimatorSet[gameUtil.POINT]);
                }
            }
        });
        gameUtil.ANIMATION_STOP = false;
        makeView();
        updateBackground();
        if(gameUtil.LEVEL==15){
            gameUtil.gameDialog.show();
            gameUtil.gameDialog.is_finish(false);
            gameUtil.gameDialog.setDesc("New Item!");
            gameUtil.gameDialog.setImg("GAMESTART","STAGE4");
        }

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setX(0);
        mAdView.setY(gameUtil.DEVICE_HEIGHT-mAdView.getHeight());

        gameUtil.setScrollviewParams();
    }
    public void goToSky(final int count){
        if(gameUtil.POINT < gameUtil.BALL_NUMBER && count < gameUtil.ITEM_SKY_MAX){
            gameUtil.textViews[gameUtil.POINT].setBackground(gameUtil.d_arr[0]);
            gameUtil.textViews[gameUtil.POINT].bringToFront();
            gameUtil.startPointAnimation(gameUtil.textViews[gameUtil.POINT].getX(),  gameUtil.textViews[gameUtil.POINT].getY(),true);
            int x = gameUtil.device_width/2 - gameUtil.ball_sizse/2;
            int y = gameUtil.ball_sizse*(-2);
            int speed = (int)( Math.sqrt( (x)*(x) + (y)*(y) ) * (2));//1px당 2ms
            ObjectAnimator translationX = ObjectAnimator.ofFloat(gameUtil.textViews[gameUtil.POINT], "translationX", x);
            translationX.setInterpolator(new LinearInterpolator());
            ObjectAnimator translationY = ObjectAnimator.ofFloat(gameUtil.textViews[gameUtil.POINT], "translationY", y);
            translationY.setInterpolator(new LinearInterpolator());
            if(gameUtil.viewAnimatorSet[gameUtil.POINT] != null){
                gameUtil.viewAnimatorSet[gameUtil.POINT].removeAllListeners();
            }
            gameUtil.viewAnimatorSet[gameUtil.POINT] = new AnimatorSet();
            gameUtil.viewAnimatorSet[gameUtil.POINT].playTogether(translationX, translationY);
            gameUtil.viewAnimatorSet[gameUtil.POINT].addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    animation.removeAllListeners();
                    gameUtil.removeBallView(gameUtil.textViews[gameUtil.POINT]);
                    gameUtil.POINT++;
                    gameUtil.setScore(gameUtil.scoreView,gameUtil.GOAL_POINT * gameUtil.POINT_VAL, gameUtil.POINT * gameUtil.POINT_VAL);
                    gameUtil.checkNextLevel();
                    makeItemView();
                    goToSky(count+1);
                }
            });
            gameUtil.viewAnimatorSet[gameUtil.POINT].setDuration(speed);
            gameUtil.viewAnimatorSet[gameUtil.POINT].start();
        }else{
            gameUtil.resumeAnimation();
            makeView();
            makeItemView();
            gameUtil.IS_GO_TO_SKY = false;
            updateBackground();
        }
    }
    public void updateBackground(){
        if(gameUtil.textViews[gameUtil.POINT] != null){
            gameUtil.textViews[gameUtil.POINT].setBackgroundResource(R.drawable.circle_game_selected);
            if( gameUtil.textViews[gameUtil.POINT+1] != null){
                gameUtil.textViews[gameUtil.POINT+1].setBackgroundResource(R.drawable.circle_game_selected2);
            }
        }
    }

    public void makeItemView(){
        int item = (int)(Math.random()*gameUtil.ITEM_PERCENTAGE);
        if(item >= 5) return;
        gameUtil.itemViews[gameUtil.ITEM_NUMBER] = new TextView(this);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].bringToFront();
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTypeface(gameUtil.typeFace);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setClickable(true);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setFocusable(true);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setWidth(gameUtil.ball_sizse);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setHeight(gameUtil.ball_sizse);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTextSize(10.0f * gameUtil.density_scale);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setGravity(Gravity.CENTER_VERTICAL);
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setId(gameUtil.ITEM_NUMBER);
        if(item == 0){
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setBackground(gameUtil.d_arr[1]);
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setText("item1");
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTextColor(Color.argb(1,0,0,0));
        }else if(item == 1 ){
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setBackground(gameUtil.d_arr[2]);
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setText("item2");
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTextColor(Color.argb(1,0,0,0));
        }else if(item == 2 ){
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setBackground(gameUtil.d_arr[3]);
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setText("item3");
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTextColor(Color.argb(1,0,0,0));
        }else if(item == 3){
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setBackground(gameUtil.d_arr[4]);
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setText("item4");
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTextColor(Color.argb(1,0,0,0));
        }else if(item == 4){
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setBackground(gameUtil.d_arr[5]);
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setText("item5");
            gameUtil.itemViews[gameUtil.ITEM_NUMBER].setTextColor(Color.argb(1,0,0,0));
        }
        gameUtil.itemViews[gameUtil.ITEM_NUMBER].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"CLICK_ITEM");

                if(gameUtil.itemViews[v.getId()] != null){
                    v.setOnClickListener(null);
                    gameUtil.setVibrate(gameUtil.VIBRATE_WEEK);
                    TextView view = (TextView) v;
                    Log.d(TAG,view.getText().toString());
                    if(view.getText().toString().equalsIgnoreCase("item1")){
                        int item_count = Integer.parseInt(gameUtil.itemBox[0].getText().toString());
                        gameUtil.itemBox[0].setText(Integer.toString(item_count+1));
                        gameUtil.scaleAnim(gameUtil.itemBox[0]);
                    }else  if(view.getText().toString().equalsIgnoreCase("item2")){
                        int item_count = Integer.parseInt(gameUtil.itemBox[1].getText().toString());
                        gameUtil.itemBox[1].setText(Integer.toString(item_count+1));
                        gameUtil.scaleAnim(gameUtil.itemBox[1]);
                    }else  if(view.getText().toString().equalsIgnoreCase("item3")){
                        int item_count = Integer.parseInt(gameUtil.itemBox[2].getText().toString());
                        gameUtil.itemBox[2].setText(Integer.toString(item_count+1));
                        gameUtil.scaleAnim(gameUtil.itemBox[2]);
                    }else  if(view.getText().toString().equalsIgnoreCase("item4")){
                        int item_count = Integer.parseInt(gameUtil.itemBox[3].getText().toString());
                        gameUtil.itemBox[3].setText(Integer.toString(item_count+1));
                        gameUtil.scaleAnim(gameUtil.itemBox[3]);
                    }else if(view.getText().toString().equalsIgnoreCase("item5")){
                        for(int i = 0; i < 5; i++){
                            if(gameUtil.POINT < gameUtil.BALL_NUMBER){
                                gameUtil.bomb(gameUtil.textViews[gameUtil.POINT]);
                                gameUtil.POINT++;
                            }else{
                                break;
                            }
                        }
                        gameUtil.setScore(gameUtil.scoreView,gameUtil.GOAL_POINT * gameUtil.POINT_VAL, gameUtil.POINT * gameUtil.POINT_VAL);
                        if(gameUtil.POINT >= gameUtil.BALL_NUMBER){
                            makeView();
                        }
                        makeItemView();
                        if(gameUtil.textViews[gameUtil.POINT] != null){
                            updateBackground();
                            if(gameUtil.USE_INVERSE_ITEM > 0){
                                gameUtil.inverseItem(gameUtil.textViews[gameUtil.POINT],gameUtil.POINT, gameUtil.viewAnimatorSet[gameUtil.POINT]);
                                gameUtil.USE_INVERSE_ITEM--;
                            }
                        }
                        gameUtil.checkNextLevel();
                    }
                    gameUtil.removeBallView(view);
                }
                return;
            }
        });
        gameUtil.addBallView(gameUtil.itemViews[gameUtil.ITEM_NUMBER], true, 1.0f);
        gameUtil.ITEM_NUMBER++;
    }
    public void makeView(){
        int count = (int)(Math.random()*(gameUtil.BALL_COUNT_LIMIT + (gameUtil.POINT/gameUtil.BALL_COUNT_PLUS_FREQUENCY))) + 1;
        for(int i = 0; i< count; i++){
            gameUtil.textViews[gameUtil.BALL_NUMBER] = new TextView(this);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setTypeface(gameUtil.typeFace);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setClickable(true);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setFocusable(true);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setWidth(gameUtil.ball_sizse);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setHeight(gameUtil.ball_sizse);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setBackgroundResource(R.drawable.circle_game_not_selected);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setGravity(Gravity.TOP);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setPadding(0,10,0,0);
            gameUtil.textViews[gameUtil.BALL_NUMBER].setId(gameUtil.BALL_NUMBER);

            gameUtil.textViews[gameUtil.BALL_NUMBER].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG,"CLICK");
                    TextView view = (TextView) v;
                    if(gameUtil.IS_GO_TO_SKY == true) return;
                    if(gameUtil.textViews[v.getId()] != null){
                        if((view.getId() - gameUtil.POINT) <= 1){
                            if((view.getId() - gameUtil.POINT) == 1){
                                gameUtil.textViews[gameUtil.POINT+1] = gameUtil.textViews[gameUtil.POINT];
                                gameUtil.textViews[gameUtil.POINT] = view;
                            }
                            gameUtil.startPointAnimation(v.getX(), v.getY(),true);
                            gameUtil.textViews[gameUtil.POINT].setOnClickListener(null);
                            gameUtil.setVibrate(gameUtil.VIBRATE_WEEK);
                            gameUtil.removeBallView(gameUtil.textViews[gameUtil.POINT]);
                            gameUtil.POINT++;
                            gameUtil.setScore(gameUtil.scoreView,gameUtil.GOAL_POINT * gameUtil.POINT_VAL, gameUtil.POINT * gameUtil.POINT_VAL);
                            makeView();
                            makeItemView();
                            if(gameUtil.textViews[gameUtil.POINT] != null){
                                updateBackground();
                                if(gameUtil.USE_INVERSE_ITEM > 0){
                                    gameUtil.inverseItem(gameUtil.textViews[gameUtil.POINT],gameUtil.POINT, gameUtil.viewAnimatorSet[gameUtil.POINT]);
                                    gameUtil.USE_INVERSE_ITEM--;
                                }
                            }
                            gameUtil.checkNextLevel();
                        }else{
                            gameUtil.setVibrate(gameUtil.VIBRATE_STRONG);
                            if(gameUtil.IS_PAUSE_BALL == true){
                                gameUtil.IS_PAUSE_BALL = false;
                                gameUtil.resumeAnimation();
                            }else{
                                gameUtil.startPointAnimation(v.getX(), v.getY(),false);
                                int item_count = Integer.parseInt(gameUtil.itemBox[3].getText().toString());
                                item_count--;
                                gameUtil.itemBox[3].setText(Integer.toString(item_count));
                                if(item_count <= 0){
                                    Toast.makeText(getApplicationContext(), "Game end!", Toast.LENGTH_LONG).show();
                                    gameUtil.gameEnd();
                                    finish();
                                }
                            }
                        }
                    }
                    return;
                }
            });
            gameUtil.addBallView(gameUtil.textViews[gameUtil.BALL_NUMBER], false, 1.0f);
            gameUtil.BALL_NUMBER++;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameUtil.resumeAnimation();
        if(((Menu)Menu.context) != null)
        ((Menu)Menu.context).startMusic();
    }
    @Override
    protected void onPause() {
        super.onPause();
        gameUtil.pauseAnimation();
        if(((Menu)Menu.context) != null)
        ((Menu)Menu.context).stopMusic();
    }
    @Override
    protected void onStop() {
        super.onStop();
        gameUtil.pauseAnimation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gameUtil.gameEnd();
        System.gc();
    }
    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.dropdown, R.anim.dropdown_exit);
    }
}
