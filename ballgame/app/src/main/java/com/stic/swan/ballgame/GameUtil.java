package com.stic.swan.ballgame;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.swan.ballgame.R;

/**
 * Created by swan on 2018-03-22.
 */
public class GameUtil {
    String TAG = "STIC";
    private Context context;
    int IS_VIBRATE_SETTING = 1;
    boolean IS_GO_TO_SKY = false;
    boolean IS_PAUSE_BALL = false;
    int USE_INVERSE_ITEM;
    int POINT_VAL = 10;//1POINT 당 10점
    int GOAL_POINT;
    int ITEM_VAL = 50;//1아이템 당 50점
    Typeface typeFace;
    float BALL_SPEED = 1.5f;
    int BALL_COUNT_LIMIT = 1;
    int ITEM_PERCENTAGE = 30;
    int BALL_COUNT_PLUS_FREQUENCY = 20;//15개 주기로 BALL CREATE ++
    int VIBRATE_WEEK = 50,
            VIBRATE_MID = 100,
            VIBRATE_STRONG = 300;
    int ITEM_SKY_MAX = 5;
    int device_width, device_height, ball_sizse;
    int TITLE_HEIGHT;
    int LEVEL = 1;
    int POINT = 0;
    int BALL_NUMBER = 0;
    int ITEM_NUMBER = 0;
    float density_scale;
    Vibrator vibrator;
    RelativeLayout mainLayout;
    TextView textViews[] = new TextView[1000];
    TextView itemViews[] = new TextView[1000];

    TextView itemBox[] = new TextView[4];
    TextView scoreView;
    TextView pointAnimation;

    AnimatorSet viewAnimatorSet[] = new AnimatorSet[1000];
    AnimatorSet itemviewAnimatorSet[] = new AnimatorSet[1000];

    GameDialog gameDialog;
    boolean ANIMATION_STOP;
    int DEVICE_WIDTH, DEVICE_HEIGHT, BALL_SIZE;

    BitmapDrawable d_arr[] = new BitmapDrawable[10];

    public GameUtil(@NonNull Context context) {
        this.context = context;
    }
    public void inverseItem(TextView view, final int index, final AnimatorSet animatorSet){
        if(view == null) return;
        if(animatorSet != null){
            animatorSet.pause();
        }
        view.bringToFront();
        view.animate()
                .scaleX(1.5f)
                .scaleY(1.5f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(animatorSet != null){
                            animatorSet.resume();
                        }
                    }
                })
                .start();
    }
    public void scaleAnim(final TextView view){//icon 사용시 scale
        view.clearAnimation();
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(view, "scaleX", 1.2f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(view, "scaleY", 1.2f);

        scaleDownX.setDuration(200);
        scaleDownY.setDuration(200);

        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.playTogether(scaleDownX, scaleDownY);
        scaleDown.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                ObjectAnimator scaleUpX = ObjectAnimator.ofFloat(view, "scaleX", 1.f);
                ObjectAnimator scaleUpY = ObjectAnimator.ofFloat(view, "scaleY", 1.f);
                scaleUpX.setDuration(200);
                scaleUpY.setDuration(200);
                AnimatorSet scaleUp = new AnimatorSet();
                scaleUp.playTogether(scaleUpX, scaleUpY);
                scaleUp.start();
            }
        });
        scaleDown.start();
    }

    public void setScore(final TextView view, final int goalPoint, final int point){
        view.setText( goalPoint + " / " + point);
    }
    public void startAnimation(final TextView view, int start_x, int start_y){
        if(ANIMATION_STOP) return;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        } else {
//            view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }
        int rand1 = (int)(Math.random()*2);
        int rand2 = (int)(Math.random()*2);
        int valx,valy;
        if(rand1 == 0){
            if(rand2 == 0){
                valx = 0;
                valy = (int)(Math.random()*(device_height - ball_sizse - TITLE_HEIGHT)) + TITLE_HEIGHT;
            }else{
                valx = device_width - ball_sizse;
                valy = (int)(Math.random()*(device_height - ball_sizse - TITLE_HEIGHT)) + TITLE_HEIGHT;
            }
        }else{
            if(rand2 == 0){
                valx = (int)(Math.random()*(device_width - ball_sizse));
                valy = TITLE_HEIGHT;
            }else{
                valx = (int)(Math.random()*(device_width - ball_sizse));
                valy = device_height - ball_sizse;
            }
        }
        if(start_x == valx && valx == 0){
            valx = device_width - ball_sizse;
        }
        if(start_x == valx && valx == device_width - ball_sizse){
            valx = 0;
        }
        if(start_y == valy && valy == TITLE_HEIGHT){
            valy = device_height - ball_sizse;
        }
        if(start_y == valy && valy == device_height - ball_sizse){
            valy = TITLE_HEIGHT;
        }

        final int x = valx;
        final int y = valy;
//        final int x = (int)(Math.random()*(device_width - ball_sizse));
//        final int y = (int)(Math.random()*(device_height - ball_sizse - TITLE_HEIGHT)) + TITLE_HEIGHT;

        //Log.d("test","BALL x : " + (int) x +", y : " + (int)y);

        int speed = (int)( Math.sqrt( (x-start_x)*(x-start_x) + (y-start_y)*(y-start_y) ) * BALL_SPEED);//1px당 2ms
        ObjectAnimator translationX = ObjectAnimator.ofFloat(view, "translationX", x);
        translationX.setInterpolator(new LinearInterpolator());
        ObjectAnimator translationY = ObjectAnimator.ofFloat(view, "translationY", y);
        translationY.setInterpolator(new LinearInterpolator());

        viewAnimatorSet[view.getId()] = new AnimatorSet();
        viewAnimatorSet[view.getId()].playTogether(translationX, translationY);
        viewAnimatorSet[view.getId()].addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animation.removeAllListeners();
                if(ANIMATION_STOP) return;
                startAnimation(view, x, y);
            }
        });

        viewAnimatorSet[view.getId()].setDuration(speed);
        viewAnimatorSet[view.getId()].start();
    }

    public void startAnimation_item(final TextView view, int start_x, int start_y){
        if(ANIMATION_STOP) return;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        } else {
//            view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }

        int rand1 = (int)(Math.random()*2);
        int rand2 = (int)(Math.random()*2);
        int valx,valy;
        if(rand1 == 0){
            if(rand2 == 0){
                valx = 0;
                valy = (int)(Math.random()*(device_height - ball_sizse - TITLE_HEIGHT)) + TITLE_HEIGHT;
            }else{
                valx = device_width - ball_sizse;
                valy = (int)(Math.random()*(device_height - ball_sizse - TITLE_HEIGHT)) + TITLE_HEIGHT;
            }
        }else{
            if(rand2 == 0){
                valx = (int)(Math.random()*(device_width - ball_sizse));
                valy = TITLE_HEIGHT;
            }else{
                valx = (int)(Math.random()*(device_width - ball_sizse));
                valy = device_height - ball_sizse - TITLE_HEIGHT;
            }
        }
        final int x = valx;
        final int y = valy;


//        final int x = (int)(Math.random()*(device_width - ball_sizse));
//        final int y = (int)(Math.random()*(device_height - ball_sizse - TITLE_HEIGHT)) + TITLE_HEIGHT;

        //Log.d("test","ITEM x : " + (int) x +", y : " + (int)y);

        int speed = (int)( Math.sqrt( (x-start_x)*(x-start_x) + (y-start_y)*(y-start_y) ) * BALL_SPEED);//1px당 2ms
        ObjectAnimator translationX = ObjectAnimator.ofFloat(view, "translationX", x);
        translationX.setInterpolator(new LinearInterpolator());
        ObjectAnimator translationY = ObjectAnimator.ofFloat(view, "translationY", y);
        translationY.setInterpolator(new LinearInterpolator());

        itemviewAnimatorSet[view.getId()] = new AnimatorSet();
        itemviewAnimatorSet[view.getId()].playTogether(translationX, translationY);
        itemviewAnimatorSet[view.getId()].addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animation.removeAllListeners();
                if(ANIMATION_STOP) return;
                startAnimation_item(view, x, y);
            }
        });
        itemviewAnimatorSet[view.getId()].setDuration(speed);
        itemviewAnimatorSet[view.getId()].start();
    }

    public void addBallView(TextView view, final boolean is_item, float scale){
        mainLayout.addView(view);
        final TextView addedView = view;
        final int start_x = device_width/2 - ball_sizse/2,
                start_y = device_height/2 - ball_sizse/2;
        view.setX(start_x);
        view.setY(start_y);
        view.setScaleX(0.1f);
        view.setScaleY(0.1f);
        view.animate()
                .scaleX(scale)
                .scaleY(scale)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(is_item){
                            startAnimation_item(addedView, start_x, start_y);
                        }else{
                            startAnimation(addedView, start_x, start_y);
                        }

                    }
                })
                .start();
    }
    public void saveScore() {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("LEVEL"+Integer.toString(LEVEL), Activity.MODE_PRIVATE);
        int saved_score = pref.getInt("score", 0);
        int current_score = POINT*POINT_VAL;
        if(saved_score < current_score){
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("score", current_score);
            editor.commit();
        }
    }
    public String loadCharacter() {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("GAME", Activity.MODE_PRIVATE);
        String character = pref.getString("CHARACTER", "SWAN");
        return character;

    }
    public void saveLevel() {
        SharedPreferences pref = ((Activity)context).getSharedPreferences("GAME", Activity.MODE_PRIVATE);
        int level = pref.getInt("LEVEL", 0);
        if(level < LEVEL){
            UserInfo userInfo = new UserInfo(context);

//            String params = "name="+ userInfo.getUsername()+"&" +
//                    "id="+ userInfo.getUserid()+"&"+
//                    "image="+ userInfo.getUserimage()+"&"+
//                    "stage="+LEVEL;
//
//            Network  network = new Network();
//            NetworkCallback networkCallback = new NetworkCallback() {
//                @Override
//                public void callbackMethod(String code, String resultData) {
//                    if(code.equalsIgnoreCase("success")){
//                        Toast.makeText(context, "Save Complete!", Toast.LENGTH_LONG).show();
//                    }else{
//                        Toast.makeText(context, "Network error", Toast.LENGTH_LONG).show();
//                    }
//                }
//            };
//            network.requestURL(network.SET_USER_DATA + params, networkCallback);


            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("LEVEL", LEVEL);
            editor.commit();
        }
    }
    public void setGameLevel(){
        switch (LEVEL){
            case 1 : BALL_SPEED = 2.0f;
                BALL_COUNT_LIMIT = 1;
                ITEM_PERCENTAGE = 30;
                break;
            case 2 : BALL_SPEED = 1.8f;
                BALL_COUNT_LIMIT = 1;
                ITEM_PERCENTAGE = 30;
                break;
            case 3 : BALL_SPEED = 1.8f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 30;
                break;
            case 4 : BALL_SPEED = 1.5f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 30;
                break;
            case 5 : BALL_SPEED = 1.5f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 35;
                break;
            case 6 : BALL_SPEED = 1.5f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 35;
                break;
            case 7 : BALL_SPEED = 1.5f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 35;
                break;
            case 8 : BALL_SPEED = 1.5f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 35;
                break;
            case 9 : BALL_SPEED = 1.4f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 35;
                break;
            case 10 : BALL_SPEED = 1.4f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 35;
                break;
            case 11 : BALL_SPEED = 1.4f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 40;
                break;
            case 12 : BALL_SPEED = 1.3f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 40;
                break;
            case 13 : BALL_SPEED = 1.3f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 40;
                break;
            case 14 : BALL_SPEED = 1.3f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 45;
                break;
            case 15 : BALL_SPEED = 1.3f;
                BALL_COUNT_LIMIT = 2;
                ITEM_PERCENTAGE = 45;
                break;
            case 16 : BALL_SPEED = 1.2f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 45;
                break;
            case 17 : BALL_SPEED = 1.2f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 45;
                break;
            case 18 : BALL_SPEED = 1.2f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 45;
                break;
            case 19 : BALL_SPEED = 1.2f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 50;
                break;
            case 20 : BALL_SPEED = 1.1f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 50;
                break;
            case 21 : BALL_SPEED = 1.1f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 50;
                break;
            case 22 : BALL_SPEED = 1.1f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 50;
                break;
            case 23 : BALL_SPEED = 1.0f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 50;
                break;
            case 24 : BALL_SPEED = 1.0f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 60;
                break;
            case 25 : BALL_SPEED = 1.0f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 60;
                break;
            case 26 : BALL_SPEED = 0.9f;
                BALL_COUNT_LIMIT = 3;
                ITEM_PERCENTAGE = 60;
                break;
            case 27 : BALL_SPEED = 0.9f;
                BALL_COUNT_LIMIT = 4;
                ITEM_PERCENTAGE = 70;
                break;
            case 28 : BALL_SPEED = 0.8f;
                BALL_COUNT_LIMIT = 4;
                ITEM_PERCENTAGE = 70;
                break;
            case 29 : BALL_SPEED = 0.8f;
                BALL_COUNT_LIMIT = 4;
                ITEM_PERCENTAGE = 70;
                break;
            case 30 : BALL_SPEED = 0.8f;
                BALL_COUNT_LIMIT = 4;
                ITEM_PERCENTAGE = 80;
                break;
        }
    }
    public void checkNextLevel(){
        int max = GOAL_POINT - 1;
        if(POINT > max){
            gameDialog.show();
            gameDialog.is_finish(true);
            gameDialog.setDesc("LEVEL "+ Integer.toString(LEVEL)+" Clear!");
            gameDialog.setImg("CHARACTER",loadCharacter());
             saveLevel();
        }
    }
    public void bomb(TextView view){
        final TextView removedView = view;
        view.setBackgroundResource(R.drawable.circle_game_bomb);
        view.setScaleX(1.7f);
        view.setScaleY(1.7f);
        view.bringToFront();
        view.animate()
                .scaleX(0.0f)
                .scaleY(0.0f)
                .setDuration(1000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mainLayout.removeView(removedView);
                    }
                })
                .start();
    }

    public void removeBallView(TextView view){
        final TextView removedView = view;
        view.animate()
                .scaleX(0.0f)
                .scaleY(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mainLayout.removeView(removedView);
                    }
                })
                .start();
    }

    public void gameEnd(){
        ANIMATION_STOP = true;
        scoreView.setOnTouchListener(null);
        for(int i =0; i<textViews.length; i++){
            if(itemViews[i] != null){
                itemViews[i].setOnClickListener(null);
                itemViews[i].clearAnimation();
                mainLayout.removeView(itemViews[i]);
            }
            if(textViews[i] != null){
                textViews[i].setOnClickListener(null);
                textViews[i].clearAnimation();
                mainLayout.removeView(textViews[i]);
            }
            if(viewAnimatorSet[i] != null && viewAnimatorSet[i].isRunning()) {
                viewAnimatorSet[i].end();
            }
            if(itemviewAnimatorSet[i] != null && itemviewAnimatorSet[i].isRunning()) {
                itemviewAnimatorSet[i].end();
            }
        }
        mainLayout.removeAllViews();
        saveScore();
    }
    public void pauseAnimation(){
        for(int i = 0; i<BALL_NUMBER; i++){
            if(viewAnimatorSet[i] != null && viewAnimatorSet[i].isRunning()){
                viewAnimatorSet[i].pause();
            }
            if(itemviewAnimatorSet[i] != null && itemviewAnimatorSet[i].isRunning()){
                itemviewAnimatorSet[i].pause();
            }
        }
    }
    public void resumeAnimation(){
        for(int i = 0; i<BALL_NUMBER; i++){
            if(viewAnimatorSet[i] != null && viewAnimatorSet[i].isPaused()){
                viewAnimatorSet[i].resume();
            }
            if(itemviewAnimatorSet[i] != null && itemviewAnimatorSet[i].isPaused()){
                itemviewAnimatorSet[i].resume();
            }
        }
    }
    public void makePointAnimation(){
        pointAnimation = new TextView(context);
        pointAnimation.setText("");
        pointAnimation.setBackgroundColor(Color.TRANSPARENT);
        pointAnimation.setTextColor(Color.WHITE);
        pointAnimation.setTextSize(50);
        pointAnimation.setTypeface(typeFace);
        pointAnimation.setVisibility(View.INVISIBLE);
        mainLayout.addView(pointAnimation);
    }
    public void startPointAnimation(float x, float y, boolean plus){

        Log.d(TAG,"startPointAnimation");
        if(plus){
            pointAnimation.setText("+10");
        }else{
            pointAnimation.setText("!!!!!");
        }
        y-=ball_sizse;
        pointAnimation.animate().cancel();

        if(IS_GO_TO_SKY){
            x = device_width/2;
            y -= ball_sizse;
        }
        pointAnimation.bringToFront();
        pointAnimation.setVisibility(View.VISIBLE);
        pointAnimation.setX(x);
        pointAnimation.setY(y);
        pointAnimation.animate()
                .setInterpolator(new LinearInterpolator())
                .translationX(x)
                .translationY(y-ball_sizse)
                .setDuration(1000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        pointAnimation.setVisibility(View.INVISIBLE);
                    }
                })
                .start();
    }
    public void setVibrate(int ms){
        if(IS_VIBRATE_SETTING == 1){
            vibrator.vibrate(ms);
        }
    }

    public void setScrollviewParams(){
        RelativeLayout.LayoutParams scoreViewParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        scoreViewParams.setMargins(0,(int)(device_height*0.8),0,0);
        scoreView.setLayoutParams(scoreViewParams);
        scoreView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent event)
            {
                // 터치 이벤트 제거
                return false;
            };
        });
    }
}
