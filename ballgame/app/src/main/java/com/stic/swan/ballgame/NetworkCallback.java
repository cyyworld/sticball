package com.stic.swan.ballgame;

/**
 * Created by swan on 2018-03-27.
 */

public interface NetworkCallback {
    void callbackMethod(String code, String resultData);
}
