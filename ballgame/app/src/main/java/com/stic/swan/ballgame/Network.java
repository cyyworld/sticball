package com.stic.swan.ballgame;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

/**
 * Created by swan on 2018-03-27.
 */
public class Network {
    String TAG = "STIC";
    NetworkCallback network_cb;
    String GET_USER_DATA = "http://swantobam.esy.es/get_user_data.php";
    String SET_USER_DATA = "http://swantobam.esy.es/set_user_data.php?";

    public void requestURL(String url, NetworkCallback cb){
        this.network_cb = cb;
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("password", "yjjung");
        client.post(url,params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                try {
                    String str = new String(response, "UTF-8");
                    network_cb.callbackMethod("success",str);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                network_cb.callbackMethod("error",e.getMessage());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

}
